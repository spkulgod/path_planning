import numpy as np
from numpy import inf
import copy
import math
import time
from numpy import linalg as LA

class state:
  #__slots__ = ['pos', 'h','parent']
  g = inf
  def __init__(self,pos,goal,parent):
    self.pos = pos
    self.h = LA.norm(pos - goal,1)
    self.parent = parent
  
class RobotPlanner:
  __slots__ = ['boundary', 'blocks','flag','OPEN','CLOSED','numofdirs','dR',\
      'scale','Es','Eg','epsilon','a','b','Vs','Vg','time','unsmooth']
  path = []
  ALL_h = {}
  def __init__(self, boundary, blocks):
    self.boundary = boundary
    self.blocks = blocks
    self.flag = 0
    self.a = np.array((self.boundary[0,0],self.boundary[0,1],self.boundary[0,2]))
    self.b = np.array((self.boundary[0,3],self.boundary[0,4],self.boundary[0,5]))

  #function to check if the path from oldrp to newrp undergoes collision
  def check_collision(self,oldrp,newrp):
    if( (newrp[0]-self.boundary[0,0])*(newrp[0]-self.boundary[0,3])>=0 or \
        (newrp[1]-self.boundary[0,1])*(newrp[1]-self.boundary[0,4])>=0 or \
        (newrp[2]-self.boundary[0,2])*(newrp[2]-self.boundary[0,5])>=0):
      return False
    m = (newrp[1]-oldrp[1])/(newrp[0]-oldrp[0]+10**-20)+10**-20
    l = (newrp[2]-oldrp[2])/(newrp[0]-oldrp[0]+10**-20)+10**-20
    n = (newrp[1]-oldrp[1])/(newrp[2]-oldrp[2]+10**-20)+10**-20
    for k in range(self.blocks.shape[0]):
      if( (newrp[0]-self.blocks[k,0])*(newrp[0]-self.blocks[k,3])<=0 and\
          (newrp[1]-self.blocks[k,1])*(newrp[1]-self.blocks[k,4])<=0 and\
          (newrp[2]-self.blocks[k,2])*(newrp[2]-self.blocks[k,5])<=0):
        return False
      if (oldrp[0] - self.blocks[k,0])*(newrp[0]-self.blocks[k,0])<0:
        y1 = m*(-oldrp[0]+self.blocks[k,0])+ oldrp[1]
        z1 = l*(-oldrp[0]+self.blocks[k,0])+ oldrp[2]
        if (y1-self.blocks[k,1])*(y1-self.blocks[k,4])<=0 and\
          (z1 - self.blocks[k,2] )*( z1 - self.blocks[k,5])<=0:
          return False
      if (oldrp[0] - self.blocks[k,3])*(newrp[0]-self.blocks[k,3])<0:
        y2 = m*(-oldrp[0]+self.blocks[k,3])+ oldrp[1]
        z2 = l*(-oldrp[0]+self.blocks[k,3])+ oldrp[2]
        if (y2 - self.blocks[k,1] )*( y2 - self.blocks[k,4])<=0 and\
          (z2 - self.blocks[k,2] )*( z2 - self.blocks[k,5] )<=0:
          return False
      if (oldrp[1]-self.blocks[k,1])*(newrp[1]-self.blocks[k,1])<0:
        x1 = 1/m*(-oldrp[1]+self.blocks[k,1])+ oldrp[0]
        z1 = 1/n*(-oldrp[1]+self.blocks[k,1])+ oldrp[2]
        if (x1-self.blocks[k,0] )*(x1-self.blocks[k,3])<=0 and\
          (z1-self.blocks[k,2])*(z1-self.blocks[k,5])<=0:
          return False
      if (oldrp[1]-self.blocks[k,4])*(newrp[1]-self.blocks[k,4])<0:
        x2 = 1/m*(-oldrp[1]+self.blocks[k,4])+ oldrp[0]
        z2 = 1/n*(-oldrp[1]+self.blocks[k,4])+ oldrp[2]
        if (x2 - self.blocks[k,0] )*( x2- self.blocks[k,3])<=0 and\
          (z2 - self.blocks[k,2] )*( z2 - self.blocks[k,5])<=0:
          return False
      if (oldrp[2]-self.blocks[k,2])*(newrp[2] - self.blocks[k,2])<0:
        x1 = 1/l*(-oldrp[2]+self.blocks[k,2])+ oldrp[0]
        y1 = n*(-oldrp[2]+self.blocks[k,2])+ oldrp[1]
        if (x1 - self.blocks[k,0] )*( x1 - self.blocks[k,3])<=0 and\
          (y1 - self.blocks[k,1] )*( y1 - self.blocks[k,4])<=0:
          return False
      if (oldrp[2]-self.blocks[k,5])*(newrp[2] - self.blocks[k,5])<0:
        x2 = 1/l*(-oldrp[2]+self.blocks[k,5])+ oldrp[0]
        y2 = n*(-oldrp[2]+self.blocks[k,5])+ oldrp[1]
        if (x2 - self.blocks[k,0] )*( x2 - self.blocks[k,3])<=0 and\
          (y2 - self.blocks[k,1] )*( y2 - self.blocks[k,4] )<=0:
          return False
    return True

  #function to add a neighbour in the A-star algotihm
  def add_neighbor(self,i,newrp,goal): 
    Ealid = self.check_collision(i.pos,newrp)
    if Ealid and tuple(newrp) not in self.CLOSED:
      if tuple(newrp) not in self.OPEN:
        self.OPEN[tuple(newrp)] = state(newrp,goal,i.pos)
        if tuple(newrp) in self.ALL_h:
          self.OPEN[tuple(newrp)].h = self.ALL_h[tuple(newrp)]
      j = self.OPEN[tuple(newrp)]
      cost = math.sqrt(sum((i.pos - newrp)**2)) 
      if j.g > i.g+cost:
        self.OPEN[tuple(newrp)].parent = i.pos
        self.OPEN[tuple(newrp)].g = i.g+cost

  #function to plan weighted A*
  def plan(self,start,goal):
    t0 = time.time()
    newrobotpos = np.copy(start)
    if self.flag == 0:
      self.scale = 0.5
      self.numofdirs = 26
      [dX,dY,dZ] = np.meshgrid([-1,0,1],[-1,0,1],[-1,0,1])
      dR = np.vstack((dX.flatten(),dY.flatten(),dZ.flatten()))*self.scale
      dR = np.delete(dR,13,axis=1)
      self.dR = np.round(dR,decimals=1)
      self.flag = 1
      self.ALL_h[tuple(start)] =  math.sqrt(sum((start - goal)**2))
      self.OPEN = {}
      self.CLOSED = {}
      self.Eg = []
      self.Es = []
      self.time = 0

    if self.flag != 2:
      self.OPEN[tuple(start)] = state(start,goal,start)
      self.OPEN[tuple(start)].g = 0
      i = copy.deepcopy(self.OPEN[tuple(start)])
      N = 0
      while any(i.pos!=goal) and time.time()-t0<1.95:
        N = N+1
        min_val = min(self.OPEN.items(), key=lambda x: 5*x[1].h+x[1].g)
        i = self.OPEN.pop(min_val[0])
        self.CLOSED[min_val[0]] = copy.deepcopy(i)
        for k in range(self.numofdirs):
          newrp = np.round(i.pos + self.dR[:,k],decimals=1)
          self.add_neighbor(i,newrp,goal)
        if LA.norm(i.pos-goal) <self.scale*1.8:
          self.add_neighbor(i,goal,goal)
      if all(i.pos==goal):
        self.flag = 2
        print("Number of nodes expanded",len(self.CLOSED)+len(self.OPEN))
        print("Time taken",self.time+time.time()-t0)
      if self.flag != 2:
        self.time += time.time() -t0 
        return start
      self.path.append(i.pos)
      while i.g!=0:
        cost = i.g
        i = copy.deepcopy(self.CLOSED[tuple(i.parent)])
        if cost - self.CLOSED[tuple(i.parent)].g<=1:
          i = copy.deepcopy(self.CLOSED[tuple(i.parent)])
        self.path.append(i.pos)
      for i in self.CLOSED:
        self.Eg.append(i)
      for i in self.OPEN:
        self.Es.append(i)
    newrobotpos = self.path.pop()
    return newrobotpos

  #Function to plan using RTAA*
  def rtaa_plan(self,start,goal):
    t0 = time.time()
    newrobotpos = np.copy(start)
    if self.flag == 0:
      self.scale = 0.5
      self.numofdirs = 26
      [dX,dY,dZ] = np.meshgrid([-1,0,1],[-1,0,1],[-1,0,1])
      dR = np.vstack((dX.flatten(),dY.flatten(),dZ.flatten()))*self.scale
      dR = np.delete(dR,13,axis=1)
      self.dR = np.round(dR,decimals=1)
      self.flag = 1
      self.ALL_h[tuple(start)] =  math.sqrt(sum((start - goal)**2))

    if self.flag == 1:
      self.OPEN = {}
      self.CLOSED = {}
      self.OPEN[tuple(start)] = state(start,goal,start)
      self.OPEN[tuple(start)].g = 0
      i = copy.deepcopy(self.OPEN[tuple(start)])
      N = 0
      while any(i.pos!=goal) and time.time()-t0<1.95:
        N = N+1
        min_val = min(self.OPEN.items(), key=lambda x: x[1].h+x[1].g)
        i = self.OPEN.pop(min_val[0])
        self.CLOSED[min_val[0]] = copy.deepcopy(i)
        for k in range(self.numofdirs):
          newrp = np.round(i.pos + self.dR[:,k],decimals=1)
          self.add_neighbor(i,newrp,goal)
        if i.h<self.scale*1.8:
          self.add_neighbor(i,goal,goal)
      if all(i.pos==goal):
        self.flag = 2
      if self.flag!=2:
        min_val = min(self.OPEN.items(), key=lambda x: x[1].h+x[1].g)
        i = self.OPEN[min_val[0]]
      f_star = i.g+i.h
      self.path.append(i.pos)
      while i.g!=0:
        cost = i.g
        i = copy.deepcopy(self.CLOSED[tuple(i.parent)])
        if cost - self.CLOSED[tuple(i.parent)].g<=1:
          i = copy.deepcopy(self.CLOSED[tuple(i.parent)])
          cost = i.g 
        self.path.append(i.pos)
      for key in self.CLOSED:
        self.ALL_h[key] = f_star - self.CLOSED[key].g
      self.path.pop()
    newrobotpos = np.round(self.path.pop(),decimals=1)
    return newrobotpos

  #Function to setup the required variables for a bidirectional RRT
  def Setup_RRT(self,start,goal):
    self.flag=1
    self.epsilon = 0.95
    self.Es ={}
    self.Eg = {}
    self.Es[tuple(start)] = [None]*2
    self.Eg[tuple(goal)]=[None]*2
    self.Vs = [start]
    self.Vg = [goal]
    self.time = 0

  #Steer function for sampling based planning
  def steer(self,x_rand,x_nearest):
    if LA.norm(x_nearest - x_rand)>self.epsilon:
      x_new = x_nearest+self.epsilon*(x_rand-x_nearest)\
        /LA.norm(x_nearest - x_rand)
    else:
      x_new = x_rand
    return np.round(x_new,decimals=4)
  
  #Extend function for RRT-Connect
  def Extend(self,V,E,x):
    nearest = np.argmin(np.sum((V-x)**2,axis=1))
    x_nearest = np.array(V[nearest])
    x_new = self.steer(x,x_nearest) 
    if self.check_collision(x_nearest,x_new):
      E[tuple(x_new)] = x_nearest
      V.append(x_new)
      if all(x_new == x):
        return 2
      else:
        return 1
    return 0
  
  #Connect function for RRT-Connect
  def Connect(self,V,E,x):
    status = 1
    while status==1:
      status = self.Extend(V,E,x)
    return status

  #Generate paths for bidiectional trees
  def Generate_Path(self,Ea,Eb,xa_new,goal):
    i = xa_new
    self.path.append(i)
    while None not in Ea[tuple(i)]:
      i = Ea[tuple(i)]
      self.path.append(i)
    i = xa_new
    self.path.reverse()
    while None not in Eb[tuple(i)]:
      i = Eb[tuple(i)]
      self.path.append(i)
    if tuple(goal) in Eb:
      self.path.reverse()

  #Path smoothing for RRTs
  def Smoothing(self):
    self.path.reverse()
    i = 0
    j = 1
    while j+1 < len(self.path):
      while self.check_collision(self.path[i],self.path[j]) and\
          j+1 < len(self.path):
        j+=1
      j=j-1
      vec = self.epsilon*(self.path[j]-self.path[i])\
          /LA.norm(self.path[j]-self.path[i])
      while LA.norm(self.path[i] - self.path[j])>1:
        self.path[i+1] = self.path[i] + vec
        i+=1
      self.path[i+1] = self.path[j]
      i = i+1
      if i!=j:
        del self.path[i:j]
      j = i+1
    self.flag = 3
    self.path.reverse()

  #Function for bidirectional RRT
  def sampling_plan(self,start,goal):
    if self.flag==0:
      self.Setup_RRT(start,goal)
    t0 = time.time()
    if self.flag == 1:
      Ea = self.Es
      Eb = self.Eg
      Va = self.Vs
      Vb = self.Vg
      i=0
      solution = 0
      while solution==0 and time.time()-t0<1.95:
        i+=1
        x_rand = (self.b-self.a)*np.random.random_sample((3)) + self.a
        nearest = np.argmin(np.sum((Va-x_rand)**2,axis=1))
        xa_nearest = np.array(Va[nearest])
        xa_new = self.steer(x_rand,xa_nearest) 
        if self.check_collision(xa_nearest,xa_new):
          Ea[tuple(xa_new)] = xa_nearest
          Va.append(xa_new)
          nearest = np.argmin(np.sum((Vb-x_rand)**2,axis=1))
          xb_nearest = np.array(Vb[nearest])
          xb_new = self.steer(x_rand,xb_nearest) 
          if self.check_collision(xb_nearest,xb_new):
            Eb[tuple(xb_new)] = xb_nearest
            Vb.append(xb_new)
            if all(xb_new==xa_new):
              solution = 1
        if len(Eb)<len(Ea):
          temp = Ea
          Ea = Eb
          Eb = temp
          temp2 = Va
          Va = Vb
          Vb = temp2
      if solution == 1:
        self.flag = 2
      if solution == 0:
        return start
      self.Generate_Path(Ea,Eb,xa_new,goal)

    if self.flag==2:
      self.Smoothing()
      self.flag == 3

    return self.path.pop()
  
  #Function for RRT-connect
  def rrtconnect_plan(self,start,goal):
    if self.flag==0:
      self.Setup_RRT(start,goal)
    t0 = time.time()
    if self.flag == 1:
      Ea = self.Es
      Eb = self.Eg
      Va = self.Vs
      Vb = self.Vg
      i=0
      solution = 0
      while solution==0 and time.time()-t0<1.95:
        i+=1
        x_rand = (self.b-self.a)*np.random.random_sample((3)) + self.a
        if self.Extend(Va,Ea,x_rand)!=0:
          if self.Connect(Vb,Eb,Va[-1]) == 2:
            solution = 1
        temp = Ea
        Ea = Eb
        Eb = temp
        temp2 = Va
        Va = Vb
        Vb = temp2
      if solution == 1:
        self.flag = 2
      if solution == 0:
        self.time += time.time() -t0 
        return start
      self.Generate_Path(Ea,Eb,Va[-1],goal)

    if self.flag==2:
      self.unsmooth = copy.deepcopy(self.path)
      self.Smoothing()
      self.flag==3
      print("Time:", self.time+time.time()-t0)
    
    return self.path.pop()

  #Function to plan RRT*    
  def rrts_plan(self,start,goal):
    if self.flag==0:
      self.flag=1
      self.epsilon = 0.95
      self.Es ={}
      self.Vs = [start]
      self.Es[tuple(start)] = [[None]*2,0]
    t0 = time.time()
    if self.flag == 1:
      i=0
      while time.time()-t0<1.95:
        i+=1
        x_rand = (self.b-self.a)*np.random.random_sample((3)) + self.a
        if i%20==0 and self.flag==1: 
          x_rand = goal
        nearest = np.argmin(np.sum((self.Vs-x_rand)**2,axis=1))
        xa_nearest = np.array(self.Vs[nearest])
        xa_new = self.steer(x_rand,xa_nearest) 
        if self.check_collision(xa_nearest,xa_new):
          Cost = np.sqrt(np.sum((self.Vs-xa_new)**2,axis=1))
          Xnear= Cost<self.epsilon
          self.Vs.append(xa_new)
          c_min = self.Es[tuple(xa_nearest)][1]+ np.sqrt(\
              sum((xa_nearest-xa_new)**2))
          x_min = xa_nearest
          for k in np.where(Xnear)[0]:
             if self.check_collision(self.Vs[k],xa_new):
               if self.Es[tuple(self.Vs[k])][1] + Cost[k] < c_min:
                  x_min = self.Vs[k]
                  c_min = self.Es[tuple(self.Vs[k])][1] + Cost[k]
          self.Es[tuple(xa_new)] = [x_min,c_min]
          for k in np.where(Xnear)[0]:
            if self.check_collision(self.Vs[k],xa_new):
              if c_min + Cost[k] < self.Es[tuple(self.Vs[k])][1]:
                self.Es[tuple(self.Vs[k])] = [xa_new,c_min + Cost[k]]
          if all(xa_new==goal):
            self.flag = 2

      if self.flag == 1:
        return start
      i = goal
      self.path.append(i)
      while None not in self.Es[tuple(i)][0]:
        i = self.Es[tuple(i)][0]
        self.path.append(i)
      self.path.pop()  
    return self.path.pop()    


