import numpy as np
import time
import matplotlib.pyplot as plt; plt.ion()
from mpl_toolkits.mplot3d import Axes3D
from mpl_toolkits.mplot3d.art3d import Poly3DCollection
import RobotPlanner
from numpy import linalg as LA

def tic():
  return time.time()
def toc(tstart, nm=""):
  print('%s took: %s sec.\n' % (nm,(time.time() - tstart)))
  

def load_map(fname):
  mapdata = np.loadtxt(fname,dtype={'names': ('type', 'xmin', 'ymin', 'zmin', 'xmax', 'ymax', 'zmax','r','g','b'),\
                                    'formats': ('S8','f', 'f', 'f', 'f', 'f', 'f', 'f','f','f')})
  blockIdx = mapdata['type'] == b'block'
  boundary = np.array(mapdata[~blockIdx][['xmin', 'ymin', 'zmin', 'xmax', 'ymax', 'zmax','r','g','b']].tolist())
  blocks = np.array(mapdata[blockIdx][['xmin', 'ymin', 'zmin', 'xmax', 'ymax', 'zmax','r','g','b']].tolist())
  return boundary, blocks


def draw_map(boundary, blocks, start, goal):
  fig = plt.figure()
  ax = fig.add_subplot(111, projection='3d')
  hb = draw_block_list(ax,blocks)
  hs = ax.plot(start[0:1],start[1:2],start[2:],'ro',markersize=7,markeredgecolor='k',label='start')
  hg = ax.plot(goal[0:1],goal[1:2],goal[2:],'go',markersize=7,markeredgecolor='k',label='goal')  
  ax.set_xlabel('X')
  ax.set_ylabel('Y')
  ax.set_zlabel('Z')
  ax.set_xlim(boundary[0,0],boundary[0,3])
  ax.set_ylim(boundary[0,1],boundary[0,4])
  ax.set_zlim(boundary[0,2],boundary[0,5])  
  return fig, ax, hb, hs, hg

def draw_block_list(ax,blocks):
  v = np.array([[0,0,0],[1,0,0],[1,1,0],[0,1,0],[0,0,1],[1,0,1],[1,1,1],[0,1,1]],dtype='float')
  f = np.array([[0,1,5,4],[1,2,6,5],[2,3,7,6],[3,0,4,7],[0,1,2,3],[4,5,6,7]])
  clr = blocks[:,6:]/255
  n = blocks.shape[0]
  d = blocks[:,3:6] - blocks[:,:3] 
  vl = np.zeros((8*n,3))
  fl = np.zeros((6*n,4),dtype='int64')
  fcl = np.zeros((6*n,3))
  for k in range(n):
    vl[k*8:(k+1)*8,:] = v * d[k] + blocks[k,:3]
    fl[k*6:(k+1)*6,:] = f + k*8
    fcl[k*6:(k+1)*6,:] = clr[k,:]
  
  if type(ax) is Poly3DCollection:
    ax.set_verts(vl[fl])
  else:
    pc = Poly3DCollection(vl[fl], alpha=0.25, linewidths=1, edgecolors='k')
    pc.set_facecolor(fcl)
    h = ax.add_collection3d(pc)
    return h


def runtest(mapfile, start, goal, verbose = True):
  # Instantiate a robot planner
  boundary, blocks = load_map(mapfile)
  RP = RobotPlanner.RobotPlanner(boundary, blocks)
  path_cost = 0
  prevpos = np.copy(start)
  verbose = False
  # Display the environment
  if verbose:
    fig, ax, hb, hs, hg = draw_map(boundary, blocks, start, goal)  
  
  # Main loop
  robotpos = np.copy(start)
  numofmoves = 0
  success = True
  path = np.array(robotpos)
  while True:
  
    # Call the robot planner
    t0 = tic()
    newrobotpos = RP.plan(robotpos, goal)
    #newrobotpos = RP.rtaa_plan(robotpos, goal)
    #newrobotpos = RP.sampling_plan(robotpos, goal)
    #newrobotpos = RP.rrts_plan(robotpos, goal)
    #newrobotpos = RP.rrtconnect_plan(robotpos, goal)
    movetime = max(1, np.ceil((tic()-t0)/2.0))
    #print('move time: %d' % movetime)

    # See if the planner was done on time
    if movetime > 1:
      newrobotpos = robotpos-0.5 + np.random.rand(3)
    

    # Check if the commanded position is valid
    if sum((newrobotpos - robotpos)**2) > 1:
      print(newrobotpos,robotpos,sum((newrobotpos - robotpos)**2))
      print('ERROR: the robot cannot move so fast\n')
      success = False
      break
    if( newrobotpos[0] < boundary[0,0] or newrobotpos[0] > boundary[0,3] or \
        newrobotpos[1] < boundary[0,1] or newrobotpos[1] > boundary[0,4] or \
        newrobotpos[2] < boundary[0,2] or newrobotpos[2] > boundary[0,5] ):
      print('ERROR: out-of-map robot position commanded\n')
      success = False
      break
    for k in range(blocks.shape[0]):
      if( newrobotpos[0] > blocks[k,0] and newrobotpos[0] < blocks[k,3] and\
          newrobotpos[1] > blocks[k,1] and newrobotpos[1] < blocks[k,4] and\
          newrobotpos[2] > blocks[k,2] and newrobotpos[2] < blocks[k,5] ):
        print('ERROR: collision... BOOM, BAAM, BLAAM!!!\n')
        success = False
        break
    success = True
    
    # Make the move
    path = np.vstack((path,robotpos))
    robotpos = newrobotpos
    numofmoves += 1
    path_cost += LA.norm(prevpos-robotpos)
    prevpos = robotpos
    # Update plot
    if verbose:
      hs[0].set_xdata(robotpos[0])
      hs[0].set_ydata(robotpos[1])
      hs[0].set_3d_properties(robotpos[2])
      ax.plot(path[:,0],path[:,1],path[:,2])
      fig.canvas.flush_events()
      plt.show()
      
    # Check if the goal is reached
    if sum((robotpos-goal)**2) <= 0.1:
      break
  '''closed = np.asarray(RP.Eg)
  #del RP.Es[tuple(start)]
  open = np.asarray(RP.Es)
  #del RP.Eg[tuple(goal)]
  #unsmooth = np.asarray(RP.unsmooth)
  hs[0].set_xdata(start[0])
  hs[0].set_ydata(start[1])
  hs[0].set_3d_properties(start[2])
  ax.scatter(closed[:,0],closed[:,1],closed[:,2],label = 'Expanded states',s=1,color='red',alpha=1)
  ax.scatter(open[:,0],open[:,1],open[:,2],label = 'Open states',s=1,color='0.5',alpha=1)
  for i in RP.Eg:
    a = np.asarray([i,RP.Eg[i]]) 
    ax.plot(a[:,0],a[:,1],a[:,2],color = 'green')
  ax.plot(a[:,0],a[:,1],a[:,2],color = 'green', label='goal tree')
  for j in RP.Es:
    a = np.asarray([j,RP.Es[j]]) 
    ax.plot(a[:,0],a[:,1],a[:,2],color = 'red')
  print("Number of nodes explored:", len(RP.Es)+len(RP.Eg)+2)
  ax.plot(a[:,0],a[:,1],a[:,2],color = 'red', label='start tree')
  ax.plot(unsmooth[:,0],unsmooth[:,1],unsmooth[:,2],label='Unsmooth Path',color = 'black')
  ax.plot(path[:,0],path[:,1],path[:,2],label='Final Path',color = 'blue')
  fig.canvas.flush_events()
  ax.legend()
  plt.show()
  print(path_cost)
  plt.pause(10)'''
  print("Path_cost",path_cost)
  print("Number of nodes explored:", len(RP.Es)+len(RP.Eg))
  return success, numofmoves



def test_single_cube():    
  start = np.array([2.3, 2.3, 1.3])
  goal = np.array([7.0, 7.0, 6.0])
  success, numofmoves = runtest('./maps/single_cube.txt', start, goal, True)
  print('Success: %r'%success)
  print('Number of Moves: %i'%numofmoves)
  
  
def test_maze():
  start = np.array([0.0, 0.0, 1.0])
  goal = np.array([12.0, 12.0, 5.0])
  success, numofmoves = runtest('./maps/maze.txt', start, goal, True)
  print('Success: %r'%success)
  print('Number of Moves: %i'%numofmoves)
  
def test_window():
  start = np.array([0.2, -4.9, 0.2])
  goal = np.array([6.0, 18.0, 3.0])
  success, numofmoves = runtest('./maps/window.txt', start, goal, True)
  print('Success: %r'%success)
  print('Number of Moves: %i'%numofmoves)

def test_tower():
  start = np.array([2.5, 4.0, 0.5])
  goal = np.array([4.0, 2.5, 19.5])
  success, numofmoves = runtest('./maps/tower.txt', start, goal, True)
  print('Success: %r'%success)
  print('Number of Moves: %i'%numofmoves) 
 
def test_flappy_bird():
  start = np.array([0.5, 2.5, 5.5])
  goal = np.array([19.0, 2.5, 5.5])
  success, numofmoves = runtest('./maps/flappy_bird.txt', start, goal, True)
  print('Success: %r'%success)
  print('Number of Moves: %i'%numofmoves) 

def test_room():
  start = np.array([1.0, 5.0, 1.5])
  goal = np.array([9.0, 7.0, 1.5])
  success, numofmoves = runtest('./maps/room.txt', start, goal, True)
  print('Success: %r'%success)
  print('Number of Moves: %i'%numofmoves)

def test_monza():
  start = np.array([0.5, 1.0, 4.9])
  goal = np.array([3.8, 1.0, 0.1])
  success, numofmoves = runtest('./maps/monza.txt', start, goal, True)
  print('Success: %r'%success)
  print('Number of Moves: %i'%numofmoves)

if __name__=="__main__":
  print("single_cube")
  test_single_cube()
  print("maze")
  test_maze()
  print("flappy_bird")
  test_flappy_bird()
  print("monza")
  test_monza()
  print("window")
  test_window()
  print("tower")
  test_tower()
  print("room")
  test_room()
  








