#### Description:

This project presents an approach of implementing weighted-A\*, RTAA\*, RRT*, Bidirectional-RRT and RRT-Connect for a given set of 3D environments. The robot is considered to be subjected to constraints in its maximum distance traversable per time instant and the maximum amount of time between two consecutive position outputs. Different algorithms are implemented and their parameters are changed to compare their effects on the quality of the computed path and the overall ability of the algorithms to reach the goal.  

#### Contents:  

main.py - file containing the main program.   
RobotPlanner.py - file containing the functions and planning algorithms.   
Images- Folder conataining images. 

The images for A-star and RRT-Connect for all of the maps are as shown below.

![Alt text](Images/Figure_1.png?raw=true "Title")
![Alt text](Images/Figure_1_connect.png?raw=true "Title")
![Alt text](Images/Figure_2.png?raw=true "Title")
![Alt text](Images/Figure_2_connect.png?raw=true "Title")
![Alt text](Images/Figure_3.png?raw=true "Title")
![Alt text](Images/Figure_3_connect.png?raw=true "Title")
![Alt text](Images/Figure_4.png?raw=true "Title")
![Alt text](Images/Figure_4_connect.png?raw=true "Title")
![Alt text](Images/Figure_5.png?raw=true "Title")
![Alt text](Images/Figure_5_connect.png?raw=true "Title")
![Alt text](Images/Figure_6.png?raw=true "Title")
![Alt text](Images/Figure_6_connect.png?raw=true "Title")
![Alt text](Images/Figure_7.png?raw=true "Title")
![Alt text](Images/Figure_7_connect.png?raw=true "Title")
